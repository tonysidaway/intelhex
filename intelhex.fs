$ff                   constant ihx-max-bytes \ Theoretical maximum
#11 ihx-max-bytes 2* + constant ihx-max-chars

create ihx-charbuffer ihx-max-chars chars allot

( Offsets within the incoming hex data record )
#1 constant ihx-ofs-nbytes
#3 constant ihx-ofs-address
#7 constant ihx-ofs-rectype
#9 constant ihx-ofs-data

0 value ihx-charsread
-1 value ihx-current-address
-1 value ihx-current-nbytes
-1 value ihx-last-address
-1 value ihx-last-nbytes
0  value ihx-output-buffer-size
0  value ihx-output-buffer-start-address
0  value ihx-nbytes
0  value ihx-records
0 value ihx-data-pointer
0 value ihx-hex-file

: ihx-xchar
  dup dup '0' >=
    swap '9' <= and
    if '0' -
  else dup dup 'A' >=
    swap 'F' <= and
    if 'A' - #10 +
  else dup dup 'a' >=
    swap 'f' <= and
    if 'a' - #10 +
  else drop -1
  then then then ;

: ihx-compute-checksum ( u -- u ) \ Compute checksum
  0 swap 1 do
    ihx-charbuffer i + c@ ihx-xchar dup 0< abort" Non-hex character in record"
    i 1 and 0= if
      swap #16 * + +
    then
  loop ;


: ihx-check
  ihx-charbuffer c@ ':' <> abort" Record does not start with ':'"
  ihx-charsread 1 and 1 <> abort" Record has even character count"
  ihx-charsread  ihx-ofs-data 2 + < abort" Record shorter than #11 characters"
  ihx-charsread ihx-compute-checksum
  $ff and abort" checksum failure" ;

: ihx-get1 dup 1+ swap c@ ihx-xchar #16 * swap c@ ihx-xchar + ; ( 1 hex byte)
: ihx-get2 dup 2 + swap ihx-get1 #8 lshift swap ihx-get1 or ; ( 2 hex bytes )

: ihx-getrec
  ihx-charbuffer ihx-ofs-nbytes + ihx-get1 to ihx-current-nbytes
  ihx-charbuffer ihx-ofs-address + ihx-get2 to ihx-current-address

  ihx-last-nbytes 0>= if
    ihx-last-address ihx-last-nbytes + ihx-current-address <>
    abort" Non-consecutive records"
  else
    ihx-current-address to ihx-output-buffer-start-address
  then
  ihx-charbuffer ihx-ofs-data +
  ihx-charbuffer ihx-ofs-nbytes + ihx-get1
  dup to ihx-last-nbytes 0 ?do
    dup ihx-get1
    ihx-data-pointer i + c!
    #2 +
  loop
  drop ;


: ihx-readfile
  to ihx-output-buffer-size ( Grab buffer size in bytes )
  dup to ihx-data-pointer >r
  r/o open-file throw
  0 to ihx-current-nbytes
  0 to ihx-records
  -1 to ihx-last-nbytes
  begin
    dup ihx-charbuffer ihx-max-chars rot read-line throw swap to ihx-charsread
  while
    ihx-records 1 + to ihx-records
    ihx-check
    ihx-charbuffer ihx-ofs-rectype + ihx-get1
					  ( Process according to record type )
    case
      &00 of ihx-getrec			  ( 00: data record )
	ihx-current-address to ihx-last-address
	ihx-current-nbytes to ihx-last-nbytes
	ihx-nbytes ihx-current-nbytes + to ihx-nbytes
	ihx-last-nbytes ihx-data-pointer + to ihx-data-pointer
      endof
      &01 of				  ( 01: end record )
	\ cr ." Total records read: " ihx-records dec.
	\ cr ." Total bytes read: " ihx-nbytes dec.
	close-file throw r> ihx-nbytes exit
      endof
      noop                                ( silently ignore other records )
    endcase
  repeat
  close-file throw
  r> ihx-nbytes ;

\ Hex file output

#1024 Value filesize
create file-buffer filesize allot

variable address

0 Value ihx-hex-file

: ihx-digit $F and s" 0123456789ABCDEF" drop + c@ ;
: ihx-pair dup ihx-digit swap #4 rshift ihx-digit ;
: ihx-quad dup ihx-pair rot #8 rshift ihx-pair ;

: ihx-hex-out ( u*c u -- )
  dup >r 0 do
    ihx-charbuffer ihx-current-nbytes + i + c!
  loop
  r> ihx-current-nbytes + to ihx-current-nbytes ;

: ihx-address-out ( addr -- )
  ihx-quad #4 ihx-hex-out ;
: ihx-byte-out    ( addr -- )
  ihx-pair #2 ihx-hex-out ;
: ihx-checksum-out      ( -- )
  ihx-current-nbytes ihx-compute-checksum
  negate $ff and ihx-pair #2 ihx-hex-out ;


: ihx-writedata ( addr u -- )
  #0 to ihx-current-nbytes
  ':' #1 ihx-hex-out  ( Record start )
  
  dup ihx-byte-out ( Data length )
  
  address @ ihx-address-out
  
  over +                      ( End address )
  $00 ihx-byte-out  ( Record type which is $00 for data record )
  swap do
    i c@ ihx-byte-out
  loop
  ihx-checksum-out
  #10 #1 ihx-hex-out
  ihx-charbuffer ihx-current-nbytes ihx-hex-file write-file throw ;

: ihx-writefinal
  s\" :00000001FF\n" ihx-hex-file write-file throw ;

: ihx-writefile ( c-addr-u addr u u -- ) \ filename buffer bytesperline
  >r
  2swap w/o bin create-file throw to ihx-hex-file
  over + swap
  r> -rot
  do
    dup i swap ihx-writedata
    dup address +!
  dup +loop
  drop
  ihx-writefinal
  ihx-hex-file close-file throw ;

create big-buffer #1024 chars allot

: test
  s" test.hex" big-buffer #1024 ihx-readfile
  s" output.hex" 2swap #16 ihx-writefile ;
