GFORTH=gforth

intelhex: intelhex.fs
	$(GFORTH) intelhex.fs -e "test" -e "bye"
	echo Round-trip test: should be no difference between test.hex and output.hex.
	diff test.hex output.hex
clean:
	-rm -f *~ output.hex

